﻿namespace ClientApp
{
    partial class SearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.BaseMfgPartNumber = new System.Windows.Forms.Label();
            this.BasePartNumber = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.OutputFolderTextbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ChooseFolderButton = new System.Windows.Forms.Button();
            this.OutputLog = new System.Windows.Forms.TextBox();
            this.FormUpdateTimer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // BaseMfgPartNumber
            // 
            this.BaseMfgPartNumber.AutoSize = true;
            this.BaseMfgPartNumber.Location = new System.Drawing.Point(12, 19);
            this.BaseMfgPartNumber.Name = "BaseMfgPartNumber";
            this.BaseMfgPartNumber.Size = new System.Drawing.Size(159, 13);
            this.BaseMfgPartNumber.TabIndex = 23;
            this.BaseMfgPartNumber.Text = "Manufacturer Base Part Number";
            this.BaseMfgPartNumber.Click += new System.EventHandler(this.lblDigikeyPartNumber_Click);
            // 
            // BasePartNumber
            // 
            this.BasePartNumber.Location = new System.Drawing.Point(177, 12);
            this.BasePartNumber.Name = "BasePartNumber";
            this.BasePartNumber.Size = new System.Drawing.Size(240, 20);
            this.BasePartNumber.TabIndex = 20;
            this.BasePartNumber.Text = "ERJ-3EKF1003V";
            this.BasePartNumber.TextChanged += new System.EventHandler(this.BasePartNumber_TextChanged);
            // 
            // btnSearch
            // 
            this.btnSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSearch.Location = new System.Drawing.Point(28, 92);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(459, 390);
            this.btnSearch.TabIndex = 22;
            this.btnSearch.Text = "Milk Me!";
            this.btnSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.button1_Click);
            // 
            // OutputFolderTextbox
            // 
            this.OutputFolderTextbox.Location = new System.Drawing.Point(177, 48);
            this.OutputFolderTextbox.Name = "OutputFolderTextbox";
            this.OutputFolderTextbox.Size = new System.Drawing.Size(240, 20);
            this.OutputFolderTextbox.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Output Directory";
            // 
            // ChooseFolderButton
            // 
            this.ChooseFolderButton.Location = new System.Drawing.Point(96, 45);
            this.ChooseFolderButton.Name = "ChooseFolderButton";
            this.ChooseFolderButton.Size = new System.Drawing.Size(75, 23);
            this.ChooseFolderButton.TabIndex = 26;
            this.ChooseFolderButton.Text = "Browse";
            this.ChooseFolderButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ChooseFolderButton.UseVisualStyleBackColor = true;
            this.ChooseFolderButton.Click += new System.EventHandler(this.ChooseFolderButton_Click);
            // 
            // OutputLog
            // 
            this.OutputLog.Location = new System.Drawing.Point(493, 92);
            this.OutputLog.MaxLength = 65535;
            this.OutputLog.Multiline = true;
            this.OutputLog.Name = "OutputLog";
            this.OutputLog.ReadOnly = true;
            this.OutputLog.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.OutputLog.Size = new System.Drawing.Size(413, 390);
            this.OutputLog.TabIndex = 27;
            this.OutputLog.TextChanged += new System.EventHandler(this.OutputLog_TextChanged);
            // 
            // FormUpdateTimer
            // 
            this.FormUpdateTimer.Enabled = true;
            this.FormUpdateTimer.Tick += new System.EventHandler(this.FormUpdateTimer_Tick);
            // 
            // SearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 495);
            this.Controls.Add(this.OutputLog);
            this.Controls.Add(this.ChooseFolderButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.OutputFolderTextbox);
            this.Controls.Add(this.BaseMfgPartNumber);
            this.Controls.Add(this.BasePartNumber);
            this.Controls.Add(this.btnSearch);
            this.Name = "SearchForm";
            this.Text = "Udder";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SearchForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label BaseMfgPartNumber;
        private System.Windows.Forms.TextBox BasePartNumber;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox OutputFolderTextbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ChooseFolderButton;
        private System.Windows.Forms.TextBox OutputLog;
        private System.Windows.Forms.Timer FormUpdateTimer;

    }
}

