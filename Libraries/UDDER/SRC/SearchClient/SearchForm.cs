﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;



namespace ClientApp
{
    public partial class SearchForm : Form
    {
        const int LogHistoryLength = 256;
        Queue<String> LogHistory = new Queue<string>();
        StreamWriter LogFileSW;
        bool Refresh = false;

         string [] PreAttributeCSV_Headers = new string[] {"Library Path",
                                                              "Library Ref",	
                                                              "Footprint Ref 1",
                                                              "Footprint Ref 2",	
                                                              "Footprint Ref 3",	
                                                              "Footprint Path 1",	
                                                              "Footprint Path 2",	
                                                              "Footprint Path 3",
                                                              "ComponentLink1Description",
                                                              "ComponentLink1URL",
                                                              "ComponentLink2Description",	
                                                              "ComponentLink2URL",	
                                                              "Stocked",
                                                              "Manufacturer",
                                                              "Manufacturer Part Number",
                                                              "Supplier 1",
                                                              "Supplier Part Number 1",
                                                              "Description",
                                                              "Category",
                                                              "Value",
                                                              "SortValue",
                                                              "Pricing",
                                                              "Sales Status"};
            

        StreamWriter CutTapeStream;
        StreamWriter TapeReelStream;

        bool Milking = false;

        public void WriteLog(string Message)
        {
            LogHistory.Enqueue(Message);

            while (LogHistory.Count > LogHistoryLength)
            {
                string Junk = LogHistory.Dequeue();
            }

          

             if (LogFileSW != null)
                LogFileSW.WriteLine("[" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "] " + Message);

            LogFileSW.Flush();
            
        }

        public SearchForm()
        {
            InitializeComponent();
            try
            {
                LogFileSW = new StreamWriter("udder.log" + System.Guid.NewGuid().ToString() + ".txt");
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Error opening log file");
            }


                WriteLog("Udder Started..... Waiting to be milked.");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Thread MilkThread = new Thread(new ThreadStart(Milk));

            MilkThread.Start();
        }

        string GetPreAttributeCSV_Data(ClientApp.SearchWS.EDAProductInfo E, string Header)
        {
            string RetVal = "";
            switch (Header)
            {
                case "Library Path":
                    RetVal = "-";
                    break;

                case "Library Ref":
                    RetVal = "-";
                    break;

                case "Footprint Ref 1":
                    RetVal = "-";
                    break;

                case "Footprint Ref 2":
                    RetVal = "-";
                    break;

                case "Footprint Ref 3":
                    RetVal = "-";
                    break;

                case "Footprint Path 1":
                    RetVal = "-";
                    break;

                case "Footprint Path 2":
                    RetVal = "-";
                    break;

                case "Footprint Path 3":
                    RetVal = "-";
                    break;

                case "ComponentLink1Description":
                    RetVal = "Internal Datasheet";
                    break;

                case "ComponentLink1URL":
                    if (E.DataSheetList.Length != 0)
                    {
                        SearchWS.DataSheet DS = E.DataSheetList.ElementAt(0);
                        RetVal = DS.Url;
                    }
                    else
                        RetVal = "No Datasheet....";
                    break;

                case "ComponentLink2Description":
                    RetVal = "External Datasheet";
                    break;

                case "ComponentLink2URL":
                    if (E.DataSheetList.Length != 0)
                    {
                        SearchWS.DataSheet DS = E.DataSheetList.ElementAt(0);
                        RetVal = DS.Url;
                    }
                    else
                           RetVal = "No Datasheet....";
                    break;

                case "Stocked":
                    RetVal = "-";
                    break;

                case "Manufacturer":
                    RetVal = E.BaseProductInfo.ManufacturerName;
                    break;

                case "Manufacturer Part Number":
                    RetVal = E.BaseProductInfo.ManufacturerPartNumber;
                    break;

                case "Supplier 1":
                    RetVal = "Digi-Key";
                    break;

                case "Supplier Part Number 1":
                    RetVal = E.BaseProductInfo.DigiKeyPartNumber;
                    break;

                case "Description":
                    RetVal = E.BaseProductInfo.ProductDescription.Replace(',', '-');
                    break;

                case "Category":
                    RetVal = E.BaseProductInfo.Category;
                    break;

                case "SortValue":
                    RetVal = "-";
                    break;

                case "Value":
                    RetVal = "-";
                    break;

                case "Sales Status":

                    if (E.SalesInfo != null)
                    {
                        RetVal = E.SalesInfo.Description;
                    }

                    break;
                
                case "Pricing":

                    RetVal = "";
                    if (E.Pricing.PriceBreakList != null)
                    {
                        if (E.Pricing.PriceBreakList.Length > 0)
                        {
                            SearchWS.PriceBreak[] PB = E.Pricing.PriceBreakList.ToArray();

                            for (int q = 0; q < PB.Length; q++)
                            {
                                RetVal += PB[q].BreakQuantity + " = " + PB[q].UnitPrice;
                                if(q!=PB.Length-1)
                                    RetVal+="  ;  ";
                            }

                            RetVal += " (USD)";
                        }
                    }
                    else
                    {
                        RetVal = "-"; 
                    }
                    
                    break;

                default:
                    RetVal = "-"; 
                    break;
            }

            return RetVal;

        }

        void Milk()
        {
            if (BasePartNumber.Text.Trim() == "")
            {
                WriteLog("Please enter a manufacturer base part number");
  
                return;
            }

            if (OutputFolderTextbox.Text.Trim() == "")
            {
                WriteLog("Please browse to a valid output folder");
     
                return;
            }

            if (Directory.Exists(OutputFolderTextbox.Text) == false)
            {
                WriteLog("Bad output folder path!");
                
                return;
            }


            Milking = true;

          
            WriteLog("Starting to Milk info for " + BasePartNumber.Text);

           

            ClientApp.SearchWS.SearchServiceSoapClient
                client = new ClientApp.SearchWS.SearchServiceSoapClient();

            ClientApp.SearchWS.SecuritySoapHeader
                securityHeader = new ClientApp.SearchWS.SecuritySoapHeader();
            ClientApp.SearchWS.UsernameToken
                usernameToken = new ClientApp.SearchWS.UsernameToken();
            usernameToken.Username = "EDA_Altrium_TEST";
            usernameToken.Password = "GRFGVAT";

            ClientApp.SearchWS.PartnerInformationSoapHeader
            partnerInformation = new ClientApp.SearchWS.PartnerInformationSoapHeader();
            partnerInformation.PartnerID = new Guid("FFD6515B-E134-431F-9A5B-77BE9CC729A3");
            securityHeader.UsernameToken = usernameToken;
                   
        

            try
            {
               
                
                //  ClientApp.SearchWS.EDAProductInfo
                ClientApp.SearchWS.SearchOptions S = new SearchWS.SearchOptions();


                ClientApp.SearchWS.BaseProductInfoCollection
                productInfo = client.KeywordSearch(securityHeader, partnerInformation, this.BasePartNumber.Text, 100,0, S);

                int TotalRecords = productInfo.RecordCount;

                WriteLog("Search on Base part number : " + BasePartNumber.Text + " resulted in " + TotalRecords + " records returned");


                CutTapeStream = new StreamWriter(Path.Combine(OutputFolderTextbox.Text, BasePartNumber.Text + ".cut-tape.csv"), false, Encoding.GetEncoding("Windows-1252"));
                TapeReelStream = new StreamWriter(Path.Combine(OutputFolderTextbox.Text, BasePartNumber.Text + ".tape-reel.csv"), false, Encoding.GetEncoding("Windows-1252"));

               

                for(int p = 0;p<(TotalRecords/100)+1;p++)
                {
                        productInfo = client.KeywordSearch(securityHeader, partnerInformation, this.BasePartNumber.Text, 100, p*100, S);

                        for (int i = 0; i < 100; i++)
                        {

                            if ((p * 100 + i) == TotalRecords)
                            {
                                WriteLog("Finished Parsing Records");
                                break;
                            }
                            ClientApp.SearchWS.EDAProductInfo
                                j = client.GetProductInfoByDigikeyPartNumber(securityHeader, partnerInformation, productInfo.ProductList.ElementAt(i).DigiKeyPartNumber);

                            if (j == null)
                                break;

                            if (i == 0 && p == 0) // in this case figure out the column headers and write
                            {

                                WriteLog("Writing CSV Headers");

                                for (int k = 0; k < PreAttributeCSV_Headers.Length; k++)
                                {
                                    CutTapeStream.Write(PreAttributeCSV_Headers[k]);
                                    TapeReelStream.Write(PreAttributeCSV_Headers[k]);

                                    if (k != PreAttributeCSV_Headers.Length - 1)
                                    {
                                        CutTapeStream.Write(",");
                                        TapeReelStream.Write(",");
                                    }
                                }

                               
                                foreach (SearchWS.Attribute A in j.AttributeList)
                                {
                           
                                        TapeReelStream.Write("," + A.Name.Replace(',','-'));
                                        CutTapeStream.Write("," + A.Name.Replace(',','-'));
                                }

                                CutTapeStream.Write("\r\n");
                                TapeReelStream.Write("\r\n");
                            }

                            //Now we need to write the Actual Data
                
                            WriteLog("Processing part " + ((p*100)+i) + " of " + TotalRecords);

                            string PackagingType = "No Packaging Code Detected";
                            foreach (SearchWS.Attribute A in j.AttributeList)
                            {
                                if (A.Name == "Packaging")
                                {
                                    PackagingType = A.Values[0];
                                    break;
                                }
                            }
                            #region switch packing
                            switch (PackagingType)
                            {
                                case "Cut Tape (CT)":


                                    for (int k = 0; k < PreAttributeCSV_Headers.Length; k++)
                                    {
                                        string Data = GetPreAttributeCSV_Data(j, PreAttributeCSV_Headers[k]);

                                        CutTapeStream.Write(Data);
                                        
                                        if (k != PreAttributeCSV_Headers.Length - 1)
                                        {
                                            CutTapeStream.Write(",");
                                        }
                                    }

                            
                                    foreach (SearchWS.Attribute A in j.AttributeList)
                                    {
                              
                             
                                          CutTapeStream.Write("," + A.Values[0].Replace(',','-'));
                              
                                    }
                        
                                    CutTapeStream.Write("\r\n");
                                    CutTapeStream.Flush();

                                    WriteLog("---->Part Detected as Cut Tape");


                                    break;

                                case "Tape & Reel (TR)":

                                    for (int k = 0; k < PreAttributeCSV_Headers.Length; k++)
                                    {
                                        string Data = GetPreAttributeCSV_Data(j, PreAttributeCSV_Headers[k]);

                                        TapeReelStream.Write(Data);

                                        if (k != PreAttributeCSV_Headers.Length - 1)
                                        {
                                            TapeReelStream.Write(",");
                                        }
                                    }

                                    foreach (SearchWS.Attribute A in j.AttributeList)
                                    {

                                        TapeReelStream.Write("," + A.Values[0].Replace(',','-'));
                                    }

                                    TapeReelStream.Write("\r\n");
                                    TapeReelStream.Flush();
                                    WriteLog("---->Part Detected as Tape & Real");
                            
                                    break;

                                default:
                                    WriteLog("---->Part Detected as [" + PackagingType + "]   Discarding.....");
                                    break;
                            }
                            #endregion
                        }
                    }

                        WriteLog("Udder is now dry!");
                        TapeReelStream.Close();
                        CutTapeStream.Close();
                    }
                    catch (Exception exception)
                    {
                        WriteLog(exception.Message);

                        try
                        {
                            TapeReelStream.Close();
                            CutTapeStream.Close();
                        }
                        catch
                        {

                        }
                    }

            Milking = false;

               return;
     

        }

        private void lblDigikeyPartNumber_Click(object sender, EventArgs e)
        {

        }

        private void ChooseFolderButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog FBD = new FolderBrowserDialog();

            if(FBD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                 OutputFolderTextbox.Text = FBD.SelectedPath;
        }

        private void SearchForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            LogFileSW.Close();
        }

        private void BasePartNumber_TextChanged(object sender, EventArgs e)
        {

        }

        private void OutputLog_TextChanged(object sender, EventArgs e)
        {

        }

        private void FormUpdateTimer_Tick(object sender, EventArgs e)
        {
        
            string[] temp = LogHistory.ToArray();
            string [] temp_reversed = new string[temp.Length];
            for (int i = 0; i < temp.Length;i++ )
            {
                temp_reversed[i] = temp[temp.Length - i - 1];
            }

            if (temp != null)
                OutputLog.Lines = temp_reversed;

            if (Milking == true)
            {

                if (btnSearch.Text != "")
                {
                    btnSearch.Text = "";
                    btnSearch.Image = global::ClientApp.Properties.Resources.digimilk;
                }
           }
            else
            {

                btnSearch.Text = "Milk Me!";
                btnSearch.Image = null;
            }

        }
    }
}
