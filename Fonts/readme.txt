
This font is Cardware, please send a Post Card to me as thanks for
my time used to save you lots of time, tell me about you or your
company or where you live on the card. Sure send money if you like too :)

Also if I knock your door, I will expect a free beer.

	Mooretronics 
	10 Wilga Street
	Mt Waverley Vic 3149
	AUSTRALIA

	mooretronics@gmail.com

Directions, install the font file in your fonts folder, then use the
font from any True Type font list named Mooretronics.

Used characters:

A				a	
B				b
C	CE			c
D				d
E	ESD with text		e	ESD
F	FCC			f
G	Earth Ground		g	Frame Ground
H	High Voltage		h
I				i
J				j
K				k
L				l
M				m
N				n
O				o
P	Pb Free with text	p	Pb Free
Q				q
R				r
S				s
T	C-TCK			t
U	UL			u
V				v
W	WEEE			w
X				x
Y				y
Z				z

