

Var
    Board     : IPCB_Board;
    WorkSpace : IWorkSpace;

{..............................................................................}
Procedure PlaceAPCBTrack(X1, Y1, X2, Y2, Layer, Width,Name);
Var
    Track         : IPCB_Track;
    Net         : IPCB_Net;
Begin
    (* Create a Track object*)
    Track             := PCBServer.PCBObjectFactory(eTrackObject, eNoDimension, eCreate_Default);
    Track.X1          := MilsToCoord(X1);
    Track.Y1          := MilsToCoord(Y1);
    Track.X2          := MilsToCoord(X2);
    Track.Y2          := MilsToCoord(Y2);
    Track.Layer       := Layer;
    Track.Width       := MilsToCoord(Width);

    Net  := PCBServer.PCBObjectFactory(eNetObject, eNoDimension, eCreate_Default);
    Net.Name := Name;

    Track.Net := Net;

    Board.AddPCBObject(Net);
    Board.AddPCBObject(Track);
End;

Procedure PlaceATrack(X1, Y1, X2, Y2, Layer, Width);
Var
    Track         : IPCB_Track;
    Net         : IPCB_Net;
Begin
    (* Create a Track object*)
    Track             := PCBServer.PCBObjectFactory(eTrackObject, eNoDimension, eCreate_Default);
    Track.X1          := MilsToCoord(X1);
    Track.Y1          := MilsToCoord(Y1);
    Track.X2          := MilsToCoord(X2);
    Track.Y2          := MilsToCoord(Y2);
    Track.Layer       := Layer;
    Track.Width       := MilsToCoord(Width);

    Board.AddPCBObject(Track);
End;

{..............................................................................}
Procedure MakeSensor;
Var
    I            : Integer;
    SensorHeight : Integer;
    SensorWidth  : Integer;
    LineWidth    : Integer;
    LineSpace    : Integer;
    NumberFingers : Integer;
    FingerWidth : Integer;
    XOffset : Integer;
    YOffset : Interger;

Begin
    (*create a new pcb document *)
    WorkSpace := GetWorkSpace;
    If WorkSpace = Nil Then Exit;
    Workspace.DM_CreateNewDocument('PCB');

    If PCBServer = Nil Then Exit;
    Board := PCBServer.GetCurrentPCBBoard;
    If Board = Nil then exit;

    SensorHeight := 9580 - 100;
    SensorWidth :=  14580 - 100;
    LineWidth := 50 ;
    LineSpace := 100;
    YOffset := 50;
    XOffset := 50;

    NumberFingers :=  SensorHeight/(LineSpace*2);
    FingerWidth :=    SensorWidth -  LineSpace;

    For I := 0 to NumberFingers Do
    Begin

      PlaceAPCBTrack(XOffset,YOffset+I*LineSpace*2,
                     XOffset + FingerWidth,YOffset+I*LineSpace*2,
                      eTopLayer,LineWidth,'A');

    End;

    PlaceAPCBTrack(XOffset,YOffset,
                   XOffset,YOffset+NumberFingers*LineSpace*2,
                   eTopLayer,LineWidth,'A');

    For I := 0 to NumberFingers-1 Do
    Begin

      PlaceAPCBTrack(XOffset + LineSpace,YOffset+I*LineSpace*2 + LineSpace,
                     XOffset + LineSpace + FingerWidth,YOffset+I*LineSpace*2 + LineSpace,
                      eTopLayer,LineWidth,'B');

    End;

    PlaceAPCBTrack(XOffset + LineSpace + FingerWidth,YOffset + LineSpace,
                   XOffset + LineSpace + FingerWidth,YOffset+(NumberFingers-1)*LineSpace*2 + LineSpace,
                   eTopLayer,LineWidth,'B');

    PlaceATrack(0,0,
                 0, YOffset * 2 +   SensorHeight,
                 eMechanical8,10);

    PlaceATrack(0,0,
                 XOffset * 2 +   SensorWidth, 0,
                 eMechanical8,10);

    PlaceATrack(0,YOffset * 2 +   SensorHeight,
                XOffset * 2 +   SensorWidth, YOffset * 2 +   SensorHeight,
                 eMechanical8,10);

    PlaceATrack(XOffset * 2 +   SensorWidth,0,
                XOffset * 2 +   SensorWidth, YOffset * 2 +   SensorHeight,
                 eMechanical8,10);


    

End;
{..............................................................................}

{..............................................................................}


